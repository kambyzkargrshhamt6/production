#! /usr/bin/env bash

set -euo pipefail

set -x

dry_run="${DRY_RUN:-1}"

if [ "${dry_run}" == '0' ]; then
     gitlab-psql --command='DROP EXTENSION pageinspect;'
else
    echo "[Dry-run] Would have ran command: gitlab-psql --command='DROP EXTENSION pageinspect;'"
fi
