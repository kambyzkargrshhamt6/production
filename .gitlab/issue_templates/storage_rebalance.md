# Migrate large projects off file-XX-stor-gprd to file-YY-stor-gprd

## Schedule

Beginning immediately pending manager and production engineer on-call approval starting: `YYYY-MM-DD HH:mm UTC`

## Summary

Migrate the large projects currently on `file-XX-stor-gprd.c.gitlab-production.internal` (https://dashboards.gitlab.net/d/W_Pbu9Smk/storage-stats?orgId=1&refresh=30m&fullscreen&panelId=144) to `file-YY-stor-gprd.c.gitlab-production.internal` (https://dashboards.gitlab.net/d/W_Pbu9Smk/storage-stats?orgId=1&refresh=30m&fullscreen&panelId=150)

## Explanation

The git repsitory storage shard file server disk usage is high: current shard disk usage is at `<shard-usage>` of capacity as of `<date> <time-utc>`.

Several other file storage nodes have more than 50% capacity available, so instead of additional node creation, a re-balance of git repositories between shards is indicated.

## Meta

- [ ] Replace all occurrences of "`XX`" with the source gitaly shard node number.
- [ ] Replace all occurrences of "`YY`" with the destination gitaly shard node number.


## Procedure

To re-balance git repositories from one gitaly shard file system to another:

### Setup

- [ ] Install the [re-balance script](https://gitlab.com/gitlab-com/runbooks/blob/master/scripts/storage_rebalance.rb) on a system that has access to the gitaly shard systems, ideally the local workstation of a GitLab Infrastructure Engineer.

```sh
git clone git@gitlab.com:gitlab-com/runbooks.git
cd runbooks
bundle install --path=vendor/bundle
mkdir -p scripts/logs
bundle exec scripts/storage_rebalance.rb --help
```

- [ ] Export a personal access token in an environment variable on the console system host.

```sh
export GITLAB_GPRD_ADMIN_API_PRIVATE_TOKEN=CHANGEME
```


### Dry-run

- [ ] Execute a dry-run of the re-balance script to move files from `nfs-fileXX` to `nfs-fileYY`.

Do not give an amount of disk space to migrate (`--move-amount N`), so that only one single project will be migrated for the time being.

```sh
bundle exec scripts/storage_rebalance.rb nfs-fileXX nfs-fileYY --verbose --wait=10800 --max-failures=1 --dry-run=yes
```

- [ ] Verify that the dry-run execution behaved as expected (no errors).


### Begin repository replications

- [ ] Take a snapshot of the disk for `nfs-fileXX`.

If you have installed `gcloud` on your system, then the following commands may be used in a shell session:

```sh
export disk_name='file-XX-stor-gprd-data'
gcloud auth login
gcloud config set project gitlab-production
export zone=$(gcloud compute disks list --filter="name=('${disk_name}')" --format=json | jq -r '.[0]["zone"]' | cut -d'/' -f9)
echo "${zone}"
export snapshot_name=$(gcloud compute disks snapshot "${disk_name}" --zone="${zone}" --format=json | jq -r '.[0]["name"]')
echo "${snapshot_name}"
gcloud compute snapshots list --filter="name=('${snapshot_name}')" --format=json | jq -r '.[0]["status"]'
```

- [ ] Execute the re-balance script to move files from `nfs-fileXX` to `nfs-fileYY`.

Specify an amount of disk space in gigabytes (less than `16000`) to migrate. Say, 1000 gigabytes, for example:

```sh
bundle exec scripts/storage_rebalance.rb nfs-fileXX nfs-fileYY --verbose --wait=10800 --max-failures=20 --move-amount=1000 --dry-run=no | tee scripts/logs/nfs-fileXX.migration.$(date +%Y-%m-%d_%H%M).log
```

- [ ] Verify that the execution behaved as expected.  If there were errors, that is alright because the failed repository replications are logged and can be examined using this command:

```sh
find scripts/storage_migrations -name "failed_projects_*" -exec cat {} \; | jq
```

/label ~infrastructure ~C3 ~change ~toil ~"Service:Gitaly" ~"requires production access"
