<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.
-->

# Production Change

### Change Summary

Perform OS upgrade on Gitaly nodes by rebuilding the VMs on {{ .Environment }}

{+ Please substitute the environment and add any other details relevant (e.g. which hosts will be affected if going in batches) +}

### Change Details

1. **Services Impacted**  -  ~"Service::Gitaly"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - 50 + 10 minutes for each server in the batch
1. **Downtime Component** - 5 minutes for each server in the batch (only one shard with downtime at a time)

## Detailed steps for the change

### Pre-Change Steps - steps to be completed before execution of the change

*Estimated Time to Complete (mins)* - 45

- [ ] Coordinate with delivery an execution time that doesn't coincide with a deployment
- [ ] Determine which shards will be included in the batch and add a list of the target shards in a comment below
- [ ] Get the current omnibus-gitlab deployed version: `knife role attribute get {{ .Environment }}-omnibus-version omnibus-gitlab.package.version`
- [ ] Edit the scheduled pipeline {+ https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/pipeline_schedules/214/edit for gstg, TBD for gprd +} and update the `PKR_VAR_omnibus_package_version` with the value from the previous step
- [ ] Trigger the scheduled pipeline. When it finishes, note the disk image name at the end of the CI job
- [ ] Prepare a MR for https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt where you set the `os_boot_image` of the appropiate file modules, plus the `file-packertest` module, to the image name from the previous step
- [ ] Get the current filesystem ID as reported by the `file-packertest` node via `gitlab-rails console` on the console node:
   ````ruby
    Gitlab.config.repositories.storages['default']['gitaly_address'] = "tcp://file-packertest-01-stor-{{ .Environment }}.c.{{ .GCPProject }}.internal:9999" # Point to test server
    Gitlab::GitalyClient.clear_stubs! # Force the new configuration go into effect
    Gitaly::Server.new('default').send(:info).storage_statuses.map(&:filesystem_id).uniq # Note for later
    ````
- [ ] Merge the image update MR
- [ ] Make sure the new image works correctly on the test node:
  - [ ] Rebuild the test node: `tf apply -replace="module.file-packertest.google_compute_instance.instance_with_attached_disk[0]" -target module.file-packertest`
  - [ ] Wait for the test node to come back online
  - [ ] Check fstab: `cat /etc/fstab`. You should see entries for the data disk and the log disk
  - [ ] Check mounts: `df -h /var/opt/gitlab /var/log`. You should see the data disk and the log disk mounted
  - [ ] Check the omnibus-gitlab version: `dpkg -s gitlab-ee | grep Version`. It should match the target version for this change.
  - [ ] Check chef:
    - [ ] `knife node show file-packertest-01-stor-{{ .Environment }}.c.{{ .GCPProject }}.internal`
    - [ ] `knife node show file-packertest-01-stor-{{ .Environment }}.c.{{ .GCPProject }}.internal`
    - [ ] `sudo chef-client`
  - [ ] Check the Ubuntu version: `lsb_release -a` (it should match Ubuntu 20.04)
  - [ ] Check the Linux version: `uname -a` (it should match 5.4.x)
  - [ ] Get the Gitaly filesystem ID: `sudo cat /var/opt/gitlab/git-data/repositories/.gitaly-metadata`
  - [ ] Check Gitaly connectivity via `gitlab-rails console` on the console node:
    ````ruby
    Gitlab.config.repositories.storages['default']['gitaly_address'] = "tcp://file-packertest-01-stor-{{ .Environment }}.c.{{ .GCPProject }}.internal:9999" # Point to test server
    Gitlab::GitalyClient.clear_stubs! # Force the new configuration go into effect
    Gitlab::HealthChecks::GitalyCheck.check('default') # Result should be `success=true`
    Gitaly::Server.new('default').send(:info).storage_statuses.map(&:filesystem_id).uniq # Should be equal to the filesystem ID from the previous step and to the filesystem ID before the rebuild
    ````
- [ ] Create a snapshot of the boot disk of each server in the batch:
  - [ ] `gcloud --project {{ .GCPProject }} compute disks snapshot {{ .InstanceName }} --snapshot-names=os-upgrade-{{ .InstanceName }} --zone=us-east1-c` {{+ Repeat for each instance of the batch +}}
- [ ] Set label ~"change::in-progress" on this issue

### Change Steps - steps to take to execute the change

*Estimated Time to Complete (mins)* - 5 per server in the batch

- [ ] Replace {{ .InstanceName }} {{+ Repeat for each instance of the batch +}}
  - [ ] `gcloud --project {{ .GCPProject }} compute instances update {{ .InstanceName }} --no-deletion-protection --zone us-east1-c`
  - [ ] `tf apply -replace="module.file-{{ .ModuleName }}.google_compute_instance.instance_with_attached_disk[{{ .FileInstanceIndex }}]"`
- [ ] Wait for the instaces to come back online
  - [ ] `ssh {{ .InstanceName }}.c.{{ .GCPProject }}.internal "sudo gitlab-ctl status gitaly"` {{+ Repeat for each instance of the batch +}}

### Post-Change Steps - steps to take to verify the change

*Estimated Time to Complete (mins)* - 5

- [ ] Verify that chef got setup correctly on the target instances
  - [ ] `ssh {{ .InstanceName }}.c.{{ .GCPProject }}.internal "grep node_name /etc/chef/client.rb"` {{+ Repeat for each instance of the batch +}}
- [ ] Notify `@release-managers` of completion so deployments can resume
- [ ] Delete the boot disk snapshots created on the pre steps:
  - [ ] `gcloud --project {{ .GCPProject }} compute snapshots delete os-upgrade-{{ .InstanceName }}` {{+ Repeat for each instance of the batch +}}
- [ ] Confirm that the number of shards remaining for upgrade matches expectations
- [ ] If this is the final batch, check that all shards are running the expected version

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - 5 per server in the batch

- [ ] Create a revert MR for the https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt change
- [ ] Merge and apply the revert MR
- [ ] Replace the instance(s) corresponding to the target shard(s)
  - [ ] `tf apply -replace="module.file-{{ .ModuleName }}.google_compute_instance.instance_with_attached_disk[{{ .FileInstanceIndex }}]" -target module.file-{{ .ModuleName }}` {+ Repeat for each storage of the batch +}
- [ ] Wait for the instaces to come back online
  - [ ] `ssh {{ .InstanceName }}.c.{{ .GCPProject }}.internal "sudo gitlab-ctl status gitaly"` {{+ Repeat for each instance of the batch +}}

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

- Metric: Gitaly Per-Node Service Aggregated SLIs: Apdex, Error ratio, RPS
  - Location: https://dashboards.gitlab.net/d/gitaly-host-detail/gitaly-host-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment={{ .Environment }}&var-fqdn={{ .InstanceName }}.c.{{ .GCPProject }}.internal {{+ Repeat for each instance of the batch +}}
  - What changes to this metric should prompt a rollback: Degradation violating our SLOs _after_ the instance rebuild has been completed.

- Metic: Chef client errors
  - Location: https://prometheus.gprd.gitlab.net/graph?g0.expr=chef_client_error%7Btype%3D%22gitaly%22%7D%20%3D%3D%201&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h
  - What changes to this metric should prompt a rollback: Failures due to misconfiguration of the chef client

- Metric: git Service Error Ratio
  - Location: https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1&viewPanel=1941771315&var-PROMETHEUS_DS=Global&var-environment={{ .Environment }}&var-stage=main
  - What changes to this metric should prompt a rollback: Degradation violating our SLOs, but pay attention to any degradation as the effect of a single server may not be dramatic on the overall service rate

## Summary of infrastructure changes

- [ ] Does this change introduce new compute instances? No
- [ ] Does this change re-size any existing compute instances? No
- [ ] Does this change introduce any additional usage of tooling like Elastic Search, CDNs, Cloudflare, etc? No

<!--
  * If you answer yes to any of the items in this checklist, summarize below.
-->
{+Summary of the above+}

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:
- [ ] The **scheduled day and time** of execution of the change is appropriate.
- [ ] The [change plan](#detailed-steps-for-the-change) is technically accurate.
- [ ] The change plan includes **estimated timing values** based on previous testing.
- [ ] The change plan includes a viable [rollback plan](#rollback).
- [ ] The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:
- [ ] The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
- [ ] The change plan includes success measures for all steps/milestones during the execution.
- [ ] The change adequately minimizes risk within the environment/service.
- [ ] The performance implications of executing the change are well-understood and documented.
- [ ] The specified metrics/monitoring dashboards provide sufficient visibility for the change.
        - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
- [ ] The change has a primary and secondary SRE with knowledge of the details available during the change window.

## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] This issue has a criticality label (e.g. ~C1, ~C2, ~C3, ~C4) and a change-type label (e.g. ~"change::unscheduled", ~"change::scheduled") based on the [Change Management Criticalities](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-criticalities).
- [ ] This issue has the change technician as the assignee.
- [ ] Pre-Change, Change, Post-Change, and Rollback steps and have been filled out and reviewed.
- [ ] This Change Issue is linked to the appropriate Issue and/or Epic
- [ ] Necessary approvals have been completed based on the [Change Management Workflow](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-request-workflows).
- [ ] Change has been tested in staging and results noted in a comment on this issue.
- [ ] A dry-run has been conducted and results noted in a comment on this issue.
- [ ] SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
- [ ] Release managers have been informed (If needed! Cases include DB change) prior to change being rolled out. (In #production channel, mention `@release-managers` and this issue and await their acknowledgment.)
- [ ] There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive).
